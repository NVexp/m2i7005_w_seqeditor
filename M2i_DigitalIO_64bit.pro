QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    common/ostools/spcm_md5.cpp \
    common/ostools/spcm_ostools_win.cpp \
    common/spcm_lib_card.cpp \
    common/spcm_lib_data.cpp \
    common/spcm_lib_thread.cpp \
    main.cpp \
    mainwindow.cpp \
    qcpsequencedata.cpp \
    qcustomplot.cpp \
    qseqeditor.cpp \
    sb5_file/sb5_file.cpp \
    variableedgesetup.cpp

HEADERS += \
    c_header/dlltyp.h \
    c_header/errors.h \
    c_header/regs.h \
    c_header/spcerr.h \
    c_header/spcm_drv.h \
    c_header/spectrum.h \
    common/ostools/spcm_md5.h \
    common/ostools/spcm_ostools.h \
    common/ostools/spcm_oswrap.h \
    common/spcm_lib_card.h \
    common/spcm_lib_data.h \
    common/spcm_lib_thread.h \
    mainwindow.h \
    qcpsequencedata.h \
    qcustomplot.h \
    qseqeditor.h \
    sb5_file/sb5_file.h \
    variableedgesetup.h

FORMS += \
    mainwindow.ui \
    variableedgesetup.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: LIBS += -L$$PWD/c_header/ -lspcm_win64_msvcpp

INCLUDEPATH += $$PWD/c_header
DEPENDPATH += $$PWD/c_header

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/c_header/spcm_win64_msvcpp.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/c_header/libspcm_win64_msvcpp.a
