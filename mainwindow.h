#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

// ----- include standard driver header from library -----
#include "c_header/dlltyp.h"
#include "c_header/regs.h"
#include "c_header/spcerr.h"
#include "c_header/spcm_drv.h"

// ----- include of common example librarys -----
#include "common/spcm_lib_card.h"
#include "common/spcm_lib_data.h"

// ----- operating system dependent functions for thread, event, keyboard and mutex handling -----
#include "common/ostools/spcm_oswrap.h"
#include "common/ostools/spcm_ostools.h"

// ----- standard c include files -----
#include <stdio.h>
#include <stdlib.h>
#include <direct.h>
#include <filesystem>

#include "qcpsequencedata.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define PERMLVL_ALL_ZEROS 0
#define PERMLVL_ALL_ONES 1
#define PERMLVL_INDIV 2
#define PERMLVL_HIGH_Z 3

//examines what edges there are in successive 64bit chunks, makes a corresponding
//64bit word per channel and parallelize them into the output buffer

class QSeqEditor;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    char                szBuffer[1024];     // a character buffer for any messages
    ST_SPCM_CARDINFO    stCard;             // info structure of my card
    uint64              qwMemInBytes;
    void*               pvBuffer;

    std::vector<QSeqEditor*> seqEditors_in_GUI;
    std::vector<QCheckBox*> constStates_in_GUI;


private slots:
    void on_initM2i_clicked();

    void on_confM2i_clicked();

    void on_loadData_clicked();

    void on_outputM2i_clicked();

    void on_translateTest_clicked();

    void on_closeM2i_clicked();

    void on_setPermWord_clicked();

    void on_writeFileTestButton_clicked();

    void on_parseButton0_clicked();
    void on_parseButton1_clicked();
    void on_parseButton2_clicked();
    void on_parseButton3_clicked();
    void on_parseButton4_clicked();
    void on_parseButton5_clicked();
    void on_parseButton6_clicked();
    void on_parseButton7_clicked();

    void on_loadFileTestButton_clicked();

    void on_changeSeqParams_clicked();

private:
    Ui::MainWindow *ui;
    void M2iInit();
    bool M2iSetup(int64 chanMask, int64 clockSpeed, bool clockOut, bool triggerOut, long long nbLoops,  unsigned int memSize);
    //void* generateSeqData(std::vector<void*> edges);
    std::vector<int16> generateTestData(size_t chanNb, bool print);
    bool M2iLoadIntoMem_testing();
    bool M2iLoadIntoMemory(int chanNumber);
    bool M2iOutputData();

    bool M2iSetConstLvlMode(int perm_mode);
    bool M2iSetConstLvl(int actChanNb, uint16_t lvls);
    void M2iClose();

    void translateTimestampsToParallelAndPutInBuffer(int seqLength, int chanNb, std::vector< std::vector<int> >& timestamps_rise, std::vector< std::vector<int> >& timestamps_fall, int16_t* pBuffer);
    bool writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vec, const std::vector< std::vector<edgeProp> >& fallingE_Vec, const std::string& path, const std::string& name);
    bool loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec);

};
#endif // MAINWINDOW_H
