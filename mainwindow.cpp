#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>


struct linspaced {
    int N;
    double start;
    double step;
};


std::vector< std::vector<double> > recurs_replace(std::vector<linspaced> ranges, std::vector<double>& V)
{
    std::vector< std::vector<double> > result {V};
    if (ranges.size() == 0)
    {
        return result;
    }
    else
    {
        size_t s = ranges.size();
        linspaced R = ranges[s-1];
        ranges.pop_back();
        std::vector< std::vector<double> > recursed_minus_last = recurs_replace(ranges, V);
        std::vector< std::vector<double> > will_be_returned;
        for(int k = 0; k < R.N; ++k)
        {
            for (int j = 0; j<recursed_minus_last.size(); ++j)
            {
                std::vector<double> c = recursed_minus_last[j];
                c[s-1] = R.start + k*R.step;
                will_be_returned.push_back(c);
            }
        }
        return will_be_returned;
    }
}


std::string getNow()
{
    auto now = std::chrono::system_clock::now();
    auto nowT = std::chrono::system_clock::to_time_t(now);
    //use a C-string here because it was impossible to find how to use a
    //C++-style of time formatting and strftime works and is pretty convenient
    char strNow[20];
    strftime(strNow, 20, "%F %T", localtime(&nowT));
    std::string stdStrNow{ strNow };//make std::string out of C-string

    return stdStrNow;
}

template <typename T>
std::string integ_to_binStr(T a, bool comma = false)
{
    if(sizeof(a)>8)
    {
        std::cout << "Error: type size larger than 8 bytes!";
        return "?";
    }
    uint64_t M;
    uint64_t aL = (uint64_t) a;
    std::string st;

    for(int i = sizeof(T)*8-1; i>=0; --i)
    {
        M=1ULL<<i;
        aL&M? st.append("1"):st.append("0");
        if((i>0)&&(i%4==0)&&comma){ st.append(",");}
    }
    return st;
}


std::vector<int> timestampsVec_doubleToInt(const std::vector<edgeProp>& seqData, double step)
{
    std::vector<int> result;
    result.resize(seqData.size());

    unsigned int u;
    for( int k = 0; k<seqData.size(); ++k)
    {
        u = lround(seqData[k].timestamp/step);
        result[k] = u;
    }

    return result;
}

//returns the next (integer) power of 2 after the parameter i.e. log2(x)+1 if x!=2^n, otherwise n
int next_power_of_2(int A)
{
    if (A == 0)
      return 0;

    int integLog2 = (int)log2(A);
    if (1 << integLog2 == A)
        return A;
    else
        return 1 << (integLog2 +1);
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow),
    pvBuffer(nullptr)
{
    ui->setupUi(this);

    seqEditors_in_GUI.push_back(ui->chan0);
    seqEditors_in_GUI.push_back(ui->chan1);
    seqEditors_in_GUI.push_back(ui->chan2);
    seqEditors_in_GUI.push_back(ui->chan3);
    seqEditors_in_GUI.push_back(ui->chan4);
    seqEditors_in_GUI.push_back(ui->chan5);
    seqEditors_in_GUI.push_back(ui->chan6);
    seqEditors_in_GUI.push_back(ui->chan7);

    constStates_in_GUI.push_back(ui->constState_chan0);
    constStates_in_GUI.push_back(ui->constState_chan1);
    constStates_in_GUI.push_back(ui->constState_chan2);
    constStates_in_GUI.push_back(ui->constState_chan3);
    constStates_in_GUI.push_back(ui->constState_chan4);
    constStates_in_GUI.push_back(ui->constState_chan5);
    constStates_in_GUI.push_back(ui->constState_chan6);
    constStates_in_GUI.push_back(ui->constState_chan7);


    //connect all sequence editor to the others in order to synchronize their range
    for (int k = 0; k<8; ++k)
    {
        for (int l = 0; l <8; ++l)
        {
            if (l != k )
                connect(seqEditors_in_GUI[k]->axisRect()->axis(QCPAxis::AxisType::atBottom), SIGNAL(rangeChanged(const QCPRange&)), seqEditors_in_GUI[l], SLOT(setNewRange(const QCPRange&)));
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::M2iInit()
{
    std::cout << "Initialising..." << std::endl;
    // ------------------------------------------------------------------------
    // initialize card number 0 (the first card in the system), get some information and print it
    if (bSpcMInitCardByIdx(&stCard, 0))
    {
        //printf (pszSpcMPrintDocumentationLink (&stCard, szBuffer, sizeof (szBuffer)));
        printf (pszSpcMPrintCardInfo (&stCard, szBuffer, sizeof (szBuffer)));
        std::cout << std::flush;
    }
    else
    {
        nSpcMErrorMessageStdOut (&stCard, "Error: Could not open connection with M2i.7005exp DIO card\n", true);
        std::cout << std::flush;
    }

}

//available channel enable mask in 1,2,4,8,16=all
//IMPORTANT : the memory size is given in samples = time points
bool MainWindow::M2iSetup(int64 chanMask, int64 clockSpeed, bool clockOut, bool triggerOut, long long nbLoops, unsigned int memSize)
{
    //configure clock speed
    bSpcMSetupClockPLL (&stCard, MEGA(clockSpeed), clockOut);
    printf ("Sampling rate set to %.1lf MHz\n", (double) stCard.llSetSamplerate / 1000000);
    std::cout << std::flush;
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut (&stCard, "Clock PLL setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set the number of activated channels, the memory size, the number of loops
    bSpcMSetupModeRepStdLoops(&stCard, chanMask, memSize, nbLoops);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Replay mode setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set the trigger mode
    bSpcMSetupTrigSoftware(&stCard, triggerOut);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Trigger setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set digital output voltage level
    for (int i=0; i < stCard.uCfg.stDIO.lGroups; i++)
        bSpcMSetupDigitalOutput(&stCard, i, SPCM_STOPLVL_LOW, 0, 3300);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Output level setup error:\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;
    return true;
}


bool MainWindow::M2iSetConstLvlMode(int permLvlMode)
{
    switch(permLvlMode)
    {
        case PERMLVL_ALL_ZEROS:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_LOW); break;
        case PERMLVL_ALL_ONES:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_HIGH); break;
        case PERMLVL_INDIV:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_HOLDLAST); break;
        case PERMLVL_HIGH_Z:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_TRISTATE); break;
        default:
            printf("Non-valid definition for the stop level; check argument.\n");
            std::cout << std::flush;
            return false;
    }

    // check for error code
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        nSpcMErrorMessageStdOut (&stCard, "Error detected after defining constant level\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    return true;
}

bool MainWindow::M2iSetConstLvl(int actChanNb, uint16_t lvls)
{
    //the vector of constant 1s or 0s corresponding to what each channel should be set to
    std::vector<uint64_t> constLvlVec(actChanNb, 0ULL);
    for (int i=0; i<actChanNb; ++i)
    {
        if(lvls & (1<<i))
            constLvlVec[i] = 0xFFFFFFFFFFFFFFFF;
    }

    //---------------------------------

    //configure the system for the upload
    //we choose a sequence of 128samples because it is the smallest possible even for only 1 channel activated
    //for 8 channels it is 16 samples minimum
    int64 channelMask = (1 << actChanNb) - 1;
    M2iSetup(channelMask, 10, false, false, 1, 128);
    //printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    //printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;

    //allocate buffer for data transfer, containing multiplexed data later on
    qwMemInBytes = 128 * stCard.lSetChannels / 8;
    pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
    if (!pvBuffer)
    {
        nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    int16* pDoubleByteBuffer = (int16*) pvBuffer;

    //---------------------------------

    //parallelized the words (only 1s or only 0s) into int16s and put them in the allocated buffer
    //do it twice because the memory length is 128 and we work with 64bit words
    int m = 0;
    int16_t two_bytes;
    for (int K = 0; K<2; ++K)
    {
        for (int shift = 0; shift < 64; ++shift)
        {
            for(int aCh = 0; aCh < actChanNb; ++aCh)
            {
                if((uint64_t)(1ULL<<shift) & constLvlVec[aCh])
                    two_bytes += (int16_t)(1<<(m%16));
                if(m%16==15)
                {
                    //std::cout << "new int16: " << integ_to_binStr(two_bytes, true) << std::endl;
                    *pDoubleByteBuffer++ = two_bytes;
                    two_bytes = 0;
                }
                ++m;
            }
        }
    }

    //---------------------------------

    //define buffer for transfer and start DMA transfer (very short because only 128bits per channel!)
    spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);
    // check for error code
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //---------------------------------

    //set the stop level mode to HOLDLAST
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // !!!!!!! WARNING: NEEDS TO BE DONE HERE AND NOT BEFORE BECAUSE OTHER COMMANDS SEEM TO OVERRIDE THIS SOMEHOW !!!!!!!
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    M2iSetConstLvlMode(PERMLVL_INDIV);

    //---------------------------------

    //makes sure that the stop mode was able to be set to HoldLast in order to individually define ch0,1,2,..7
    int64 stopLvlMode;
    if(spcm_dwGetParam_i64(stCard.hDrv, SPC_CH0_STOPLEVEL, &stopLvlMode)!=ERR_OK)
    {
        nSpcMErrorMessageStdOut (&stCard, "Error detected querying stop level mode\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    if (stopLvlMode!=SPCM_STOPLVL_HOLDLAST)
    {
        std::cout << "The stop level does not allow each digital channel/pin to have an independant constant state\nIt needs to be mode HOLDLAST" << std::endl;
        std::cout << std::flush;
        return false;
    }
    //---------------------------------

    //run the short sequence
    if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START) != ERR_OK)
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
        spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempting output\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    //the final level of the sequence should be held by the bit/channel

    //---------------------------------

    //free allocated memory
    vFreeMemPageAligned (pvBuffer, qwMemInBytes);

    return true;

}

std::vector<int16> MainWindow::generateTestData(size_t chanNb, bool print)
{
    //now define our own data for 8 channels
    /*
         _
    ____| |________________ CH0  1pulse
         _    _
    ____| |__| |___________ CH1  2pulses
         _    _    _
    ____| |__| |__| |______ CH2  3pulses
    etc...
    */

    int8 zero = 0;
    int8 pulse = (int8)(1<<0) + (int8)(1<<4);

    //first make one std::vector per channel
    std::vector< std::vector<int8> > pulse_seq;
    pulse_seq.resize(chanNb);

    //##################### IMPORTANT #####################
    //----- for 16channels/bits active -----
    //for loop number >1, minimum buffer size is 32 int16s <=> 64B <=> 512bits (so a minimum length from 32 samples for 16ch enabled to 512 samples for 1ch enabled)
    //for loop number of 1, minimum buffer is 8 int16s <=> 16B <=> 128bits
    int nb_of_bytes_per_chan = 64; //64*8time pts = 512 samples/time pts PER CHANNEL so 32*nb of channels int16 in total
    for(int n=0; n<chanNb; ++n)
    {
        for(int m=0; m<nb_of_bytes_per_chan; ++m)
        {
            if(m<=n)
                pulse_seq[n].push_back(pulse);
            else
                pulse_seq[n].push_back(zero);
        }
    }


    /*for(int i = 0; i<chanNb; ++i)
    {
        std::cout << "\n\nchannel " << i <<'\n';
        for (int j = 0; j<64; ++j)
        {
            std::cout << integ_to_binStr(pulse_seq[i][j]) << '\n';
        }
    }*/

    //then parallelize it into one std::vector<int16> where each element contains the
    //right amount of samples for the given number of channels
    //i.e. in 1 int16 we have:
    //16ch <->  1sample for each channel
    //8ch <->  2sample for each channel
    //4ch <->  4sample for each channel
    //2ch <->  8sample for each channel
    //1ch <->  16sample for each channel
    std::vector<int16> parallel;
    parallel.resize(32*chanNb, (int16)0);

    std::vector<size_t> possible_sizes ={1,2,4,8,16};

    if(std::find(possible_sizes.begin(), possible_sizes.end(), chanNb) != possible_sizes.end())
    {
        int m = 0;
        int16 two_bytes = 0;
        for (int byte_ind=0; byte_ind < nb_of_bytes_per_chan; ++byte_ind)
        {
            for (int shift = 0; shift < 8; ++shift)
            {
                for(int ch = 0; ch < chanNb; ++ch)
                {
                    if((int8)(1<<shift) & pulse_seq[ch][byte_ind])
                        two_bytes += (int16)(1<<(m%16));
                    if(m%16==15)
                    {
                        parallel[m/16] = two_bytes;
                        if(print)
                        {
                            printf("new 16bit word full: index %d\n", m/16);
                            printf("%s\n", integ_to_binStr(two_bytes, true).c_str());
                        }
                        two_bytes = 0;
                    }
                    ++m;
                }
            }
        }
    }
    else
    {
        printf("Number of channels not available !");
    }

    return parallel;
}


// examines what edges there are in successive 64bit chunks, makes a corresponding
// 64bit word per channel and parallelize them into the output buffer
// vector timestamps_rise and timestamps_fall are just indices of edges as INTEGERS,
// i.e. the timestamps in second will be this integer multiplying by time step
void MainWindow::translateTimestampsToParallelAndPutInBuffer(int seqLength, int chanNb, std::vector< std::vector<int> >& timestamps_rise, std::vector< std::vector<int> >& timestamps_fall, int16_t* pBuffer)
{
    if (seqLength%64 != 0)
    {
        std::cout << "Error: Length of sequence should be a multiple of 64 !!" << std::endl;
        return;
    }

    //help-vectors to store transitions found within one 64 bit chunk, one vector of ints for each channel
    //std::vector< std::vector<int> > risingEdgesInChunk (chanNb);
    std::vector<int> risingEdgesInChunk;
    //std::vector< std::vector<int> > fallingEdgesInChunk (chanNb);
    std::vector<int> fallingEdgesInChunk;

    //figure out how many channels to activate in total to get a valid number (1,2,4,8,16)
    int actChanNb = next_power_of_2(chanNb);

    //vector used to store temporary 64bit sequence chunk of
    //successive logic levels for each channel before parallelizing these chunks
    //size of the vector is the number activated channels
    std::vector<uint64_t> binChunkVec;
    binChunkVec.resize(actChanNb);
    //complete the unused channels with 0s
    for (int c = chanNb; c < actChanNb; ++c)
    {
        binChunkVec[c] = 0ULL;
    }

    int start_t, stop_t, eRise, eFall, rising_nb, falling_nb, timeInd;
    int index = 0;
    int found_val;
    std::vector<int>::iterator found_ind;
    uint64_t lvl, word;
    bool lvl_changed;

    std::vector<uint64_t> lvlAtEndOfLastChunk(chanNb, 0ULL);

    //predicate for finding edges; between start and stop and after the preceding found value
    auto test_timestampInChunck = [&start_t, &stop_t, &found_val](int T){return (T>=start_t)&&(T<=stop_t)&&(T>found_val);};

    for (int i=0; i<seqLength/64; ++i )//go through the entire length of the sequence
    {
        start_t = i*64;
        stop_t = start_t+63;

        // ############### for debugging ###############
        //std::cout << "Chunk " << i << ":" << std::endl;

        for (int ch=0; ch<chanNb; ++ch) //for each channel, with a max number of channel of 16
        {
            //reset the vectors of edges index for this chunk
            risingEdgesInChunk.clear();
            fallingEdgesInChunk.clear();

            //look what edges are in the 64 cycle long time interval
            //1. find rising edges
            found_val = -1;
            do
            {
                found_ind = std::find_if(timestamps_rise[ch].begin(), timestamps_rise[ch].end(), test_timestampInChunck);
                if (found_ind != timestamps_rise[ch].end())
                {
                    found_val = *found_ind;
                    risingEdgesInChunk.push_back(found_val);
                }
            } while (found_ind != timestamps_rise[ch].end());

            found_val = -1;
            //2. find falling edges
            do
            {
                found_ind = std::find_if(timestamps_fall[ch].begin(), timestamps_fall[ch].end(), test_timestampInChunck);
                if (found_ind != timestamps_fall[ch].end())
                {
                    found_val = *found_ind;
                    fallingEdgesInChunk.push_back(found_val);
                }

            } while (found_ind != timestamps_fall[ch].end());

            // ############### for debugging ###############
            /*
            //print all rising and falling edges found in the chunk
            auto printVec = [](int K){std::cout << K << "  ";};
            std::cout << "Rising edges found: ";
            for_each(risingEdgesInChunk.begin(), risingEdgesInChunk.end(), printVec);
            std::cout << std::endl;
            std::cout << "Falling edges found: ";
            for_each(fallingEdgesInChunk.begin(), fallingEdgesInChunk.end(), printVec);
            std::cout << std::endl;
            */

            //then build up a 64bit word corresponding to these transitions
            //Least Significant Bit corresponds to time closest to 0
            eRise = 0;
            eFall = 0;
            word = 0ULL;
            lvl = lvlAtEndOfLastChunk[ch];
            //first a sanity check: cannot have a difference of more than 1 rising or 1 falling edge between
            //the amount of edges found in the chunk

            if (std::abs((double)risingEdgesInChunk.size() - (double)fallingEdgesInChunk.size() ) > 1.1)
            {
                //std::cout << "Nb of rising edges: " << risingEdgesInChunk.size() << ", Nb of falling edges: " << fallingEdgesInChunk.size() << std::endl;
                std::cout << "Error: the difference in amount of rising and falling edges is " << std::abs((double)risingEdgesInChunk.size() - (double)fallingEdgesInChunk.size()) << ". This is too high; impossible sequence !!" << std::endl;
                return;
            }

            rising_nb = risingEdgesInChunk.size();
            falling_nb = fallingEdgesInChunk.size();
            if (rising_nb == 0 && falling_nb == 0) //no edges where found at all
            {
                if(lvl)
                    word = 0xFFFFFFFFFFFFFFFF; //64 1s
                    //else word stays equal to 64 0s
            }
            else if (rising_nb == 1 && falling_nb == 0)//only one rising edge found; it means the level is low and changes to high
            {
                index = risingEdgesInChunk[0]%64;
                for (uint8_t b = index; b<64 ; ++b)
                {
                    word += 1ULL<<b;
                }
                lvlAtEndOfLastChunk[ch] = 1ULL;//remember the level at the end of the chunk for this channel
            }
            else if (rising_nb == 0 && falling_nb == 1)//only one falling edge found; it means the level is high and changes to low
            {
                index = fallingEdgesInChunk[0]%64;
                for (uint8_t b = 0; b<index ; ++b)
                {
                    word += 1ULL<<b;
                }
                lvlAtEndOfLastChunk[ch] = 0ULL;//remember the level at the end of the chunk for this channel
            }
            else //at least 1 rising and 1 falling edge found
            {
                for(int b = 0; b<64; ++b)
                {
                    timeInd = 64*i + b;
                    lvl_changed = false;

                    if(eRise < rising_nb) //check if we haven't already seen all rising edges
                    {
                        if(timeInd == risingEdgesInChunk[eRise])//if there is a rising edge at this bit
                        {
                            lvl = 1ULL; //change level to 1
                            word += (lvl<<b);
                            ++eRise;
                            lvl_changed = true;
                        }
                    }

                    if(eFall < falling_nb) //check if we haven't already seen all falling edges
                    {
                        if (timeInd == fallingEdgesInChunk[eFall]) //if there is a falling edge at this bit
                        {
                            lvl = 0ULL; //changes level to 0
                            ++eFall;
                            lvl_changed = true;
                        }
                    }

                    if(!lvl_changed) //otherwise just keep the current level
                    {
                        word += (lvl<<b);
                    }
                }
                lvlAtEndOfLastChunk[ch] = lvl;//remember the level at the end of the chunk for this channel
            }
            //finally store the obtained 64bit words for this channel
            binChunkVec[ch] = word;
        }


        // ############### for debugging ###############
        //print content of vector with 64bit chunks
        /*
        std::cout << "data in binChunkVec\n";
        for (int j=0; j<binChunkVec.size(); ++j) { std::cout << "ch" << j << ": " << integ_to_binStr(binChunkVec[j], true) << std::endl;}
        */


        //now parallelize the chanNb 64bit chunk into (64bit*chanNb/16bit =) 4*chanNb 16bit words

        if(actChanNb<=16)//if the number of channel is valid
        {
            int m = 0;
            int16_t two_bytes = 0;
            for (int shift = 0; shift < 64; ++shift)
            {
                for(int aCh = 0; aCh < actChanNb; ++aCh)
                {
                    if((uint64_t)(1ULL<<shift) & binChunkVec[aCh])
                        two_bytes += (int16_t)(1<<(m%16));
                    if(m%16==15)
                    {
                        *pBuffer++ = two_bytes; // same as *(ptr++)
                        std::cout << "new int16: " << integ_to_binStr(two_bytes, true) << std::endl;
                        two_bytes = 0;
                    }
                    ++m;
                }
            }
        }
        else
        {
            std::cout << "Number of channels not valid !" << std::endl;
        }
    }
}


//for N time points/clock cycles/samples of the digital signal
//the data is always uploaded in chunks of 2Bytes
//the number of time points defined in each chunk(=int16) is 16/Ch pts/chunk (manual page 84) where Ch is the number of activated channels (1,2,4,8,16)
//so we have:    N pts / [(16/Ch) pts/chunk] * 2Byte/chunk = N*Ch/16 chunks * 2Byte/chunk = N*Ch/8 Bytes
bool MainWindow::M2iLoadIntoMem_testing()
{
    printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;
    qwMemInBytes = stCard.llSetMemsize * stCard.lSetChannels / 8;

    //allocate buffer for data transfer, containing multiplexed data later on
    void* pvBuffer;
    pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
    if (!pvBuffer)
    {
        nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    int16* pDoubleByteBuffer = (int16*) pvBuffer;

    //we put some data into the buffer
    std::vector<int16> myData = generateTestData(stCard.lSetChannels, false);
    std::cout << "test data generated" << std::endl;

    //some sanity checks of size and channel number
    if(stCard.llSetMemsize!=512)
    {
        printf("Registered MemSize should be 512 samples!");
        std::cout << std::flush;
        return false;
    }

    if(stCard.lSetChannels!=ui->chanNumber->value())
    {
        printf("Channel number appears to be wrong!");
        std::cout << std::flush;
        return false;
    }

    std::cout << "Size of int16 vector: " << myData.size() <<std::endl;
    for(int i = 0; i<myData.size(); ++i)
    {
        *pDoubleByteBuffer++ = myData[i];
    }

    // we define the buffer for transfer and start the DMA transfer
    printf ("Starting the DMA transfer and waiting until data is in board memory\n");
    std::cout << std::flush;
    spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

    // check for error code
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    vFreeMemPageAligned(pvBuffer, qwMemInBytes);

    return true;
}

bool MainWindow::M2iLoadIntoMemory(int chanNumber)
{
    printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;

    //allocate buffer for data transfer, containing multiplexed data later on
    qwMemInBytes = stCard.llSetMemsize * stCard.lSetChannels / 8;
    void* pvBuffer;
    pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
    if (!pvBuffer)
    {
        nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    int16* pDoubleByteBuffer = (int16*) pvBuffer;

    //get the timestamps of edges from the GUI/Sequence Editor
    double s = ui->chan0->step;
    std::vector< std::vector<int> > rising;
    std::vector< std::vector<int> > falling;
    for(int j = 0; j<chanNumber; ++j)
    {
        rising.push_back(timestampsVec_doubleToInt(seqEditors_in_GUI[j]->seqData->risingEdges, s));
        falling.push_back(timestampsVec_doubleToInt(seqEditors_in_GUI[j]->seqData->fallingEdges, s));
    }

    //translate and load the corresponding data into the buffer
    translateTimestampsToParallelAndPutInBuffer(stCard.llSetMemsize, chanNumber, rising, falling, pDoubleByteBuffer);

    // we define the buffer for transfer and start the DMA transfer
    printf ("Starting the DMA transfer and waiting until data is in board memory\n");
    std::cout << std::flush;
    spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

    // check for error code after transfer
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    vFreeMemPageAligned(pvBuffer, qwMemInBytes);
    return true;
}


bool MainWindow::M2iOutputData()
{
    //send command to start outputting data
    //if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_ENABLETRIGGER | M2CMD_CARD_WAITREADY) == ERR_TIMEOUT)
    printf("Data output...\n");
    std::cout << std::flush;
    if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START) != ERR_OK)
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
        spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempting output\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    return true;
}

void MainWindow::M2iClose()
{
    // clean up and close the driver
    printf("Closing driver...\n");
    std::cout << std::flush;
    vSpcMCloseCard (&stCard);
    //vFreeMemPageAligned (pvBuffer, qwMemInBytes);
}


bool MainWindow::writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vectors, const std::vector< std::vector<edgeProp> >& fallingE_Vectors, const std::string& path, const std::string& name)
{
    if (risingE_Vectors.size() != nbActChan || fallingE_Vectors.size() != nbActChan)
    {
        std::cout << "Error: Wrong number of vectors of rising or falling edges; this should match the number of actiated channels!" << std::endl;
        return false;
    }

    //set current working directory
    std::filesystem::path p(path);
    std::filesystem::current_path(p);

    //create output stream isntance to file
    std::ofstream fileWritten;

    std::string nowStr = getNow(); //current data and time

    //fileWritten.open(nowStr + "_Sequence_" + name + ".dat", std::ios::out | std::ios::trunc);
    std::string filename = nowStr + " Sequence " + name + ".dat";
    std::replace(filename.begin(),filename.end(), ':', ';'); //IMPORTANT: Windows file system does not accept colon ':', which we replace by ';'
    fileWritten.open(filename, std::ios::out | std::ios::trunc);

    if(fileWritten.is_open())
    {
        fileWritten << "Number of activated channels:\n" << nbActChan << '\n';
        fileWritten << "Time step (ns):\n" << tStep_in_ns << '\n' << "Clock frequency (MHz): " << 1000./tStep_in_ns << '\n';

        fileWritten << "Sequence length:\n" << length << '\n' << '\n' << '\n';

        fileWritten << "RISING EDGES:\n";
        //write how many rising edges each channel has in this sequence
        fileWritten << "Number of rising edges per channel:\n";
        for (int c=0; c<nbActChan; ++c)
        {
            fileWritten << risingE_Vectors[c].size() << '\t';
        }
        fileWritten << '\n' << '\n';
        //write the time points where the edges happen
        fileWritten << "Timestamps:\n";
        for(int c = 0; c<nbActChan; ++c)
        {
            fileWritten << "Channel #" << c << '\n';
            for(int e = 0; e<risingE_Vectors[c].size(); ++e)
            {
                fileWritten << risingE_Vectors[c][e].timestamp << '\t';
            }
            fileWritten << '\n' << '\n';
        }

        fileWritten << '\n';

        fileWritten << "FALLING EDGES:\n";
        //write how many rising edges each channel has in this sequence
        fileWritten << "Number of falling edges per channel:\n";
        for (int c=0; c<nbActChan; ++c)
        {
            fileWritten << fallingE_Vectors[c].size() << '\t';
        }
        fileWritten << '\n' << '\n';
        //write the time points where the edges happen
        fileWritten << "Timestamps:\n";
        for(int c = 0; c<nbActChan; ++c)
        {
            fileWritten << "Channel #" << c << '\n';
            for(int e = 0; e<fallingE_Vectors[c].size(); ++e)
            {
                fileWritten << fallingE_Vectors[c][e].timestamp << '\t';
            }
            fileWritten << '\n' << '\n';
        }

    }
    else
    {
        std::cout << "File failed to open!" << std::endl;
        return false;
    }
    fileWritten.close();

    return true;
}

bool MainWindow::loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec)
{
    std::filesystem::current_path(filePath);

    std::ifstream inputF;
    inputF.open(filename);

    std::string line;

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the number of activated channels   
    int P = atoi(line.c_str());
    risingE_outVec.resize(P);
    fallingE_outVec.resize(P);

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the time step in ns
    double dt = atof(line.c_str());

    std::getline(inputF, line);

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the length in samples i.e. number of time points
    int L = atoi(line.c_str());

    std::string st1 = "Number of rising edges per channel";
    for (; line.substr(0, st1.size()) != st1 && !inputF.eof(); std::getline(inputF, line))
    {
    }
    //now get the next line with the numbers of edges and parse it into ints
    std::getline(inputF, line); //--> list of the number of edges on each channel

    //now take the new line as a tab-separated stream and parse it as such
    std::istringstream iss_nb;
    iss_nb.str(line);
    std::string edgeNb;
    edgeProp e {0, nullptr};
    int k = 0;

    std::getline(iss_nb, edgeNb, '\t');//there is at least one channel obviously
    do
    {
        //std::cout << atoi(edgeNb.c_str()) << ", k=" << k << std::endl;
        std::vector<edgeProp> v (atoi(edgeNb.c_str()), e);
        if(k<risingE_outVec.size())
        {
            risingE_outVec[k] = v;
        }
        ++k;
        std::getline(iss_nb, edgeNb, '\t');
    }while(!iss_nb.eof());

    std::cout << "rise outVec->" << risingE_outVec.size() << std::endl;
    for(int i = 0; i<risingE_outVec.size(); ++i)
    {
        std::cout << risingE_outVec[i].size() << std::endl;
    }

    //now we are done initializing the vectors of edges

    //skip 2 lines
    std::getline(inputF, line);
    std::getline(inputF, line);

    std::string timestamp;
    std::istringstream iss_t;

    //successively parse the lines containing the timestamps for the rising edges on each channel
    std::cout << "Rising edges first:" << std::endl;
    for(int chInd = 0; chInd<P; ++chInd)
    {
        std::getline(inputF, line); // --> line "Channel #"
        std::getline(inputF, line); // --> line with edges on chInd

        iss_t.clear();
        iss_t.str(line);
        k = 0;
        if(risingE_outVec[chInd].size()>0)//there is at least 1 edge on this channel
        {
            std::getline(iss_t, timestamp, '\t');
            do
            {
                std::cout << atof(timestamp.c_str()) << ", k=" << k << std::endl;
                if(k<risingE_outVec[chInd].size())
                    risingE_outVec[chInd][k].timestamp = atof(timestamp.c_str());
                ++k;
                std::getline(iss_t, timestamp, '\t');
            }while(!iss_t.eof());
        }
        std::getline(inputF, line);// --> empty line
    }

    //---------------------------------------------------------------------------------------
    //THEN REPEAT THE SAME CYCLES FOR THE FALLING EDGES

    std::string st2 = "Number of falling edges per channel";
    for (; line.substr(0, st2.size()) != st2 && !inputF.eof(); std::getline(inputF, line))
    {
    }

    std::cout << "Falling edges from now:" << std::endl;
    //now get the next line with the numbers of edges and parse it into ints
    std::getline(inputF, line); //--> list of the number of edges on each channel

    //now take the new line as a tab-separated stream and parse it as such
    iss_nb.clear();
    iss_nb.str(line);
    k = 0;

    std::getline(iss_nb, edgeNb, '\t');//there is at least one channel obviously
    do
    {
        //std::cout << atoi(edgeNb.c_str()) << ", k=" << k << std::endl;
        std::vector<edgeProp> v (atoi(edgeNb.c_str()), e);
        if(k<fallingE_outVec.size())
        {
            fallingE_outVec[k] = v;
        }
        ++k;
        std::getline(iss_nb, edgeNb, '\t');
    }while(!iss_nb.eof());

    //skip 2 lines
    std::getline(inputF, line);
    std::getline(inputF, line);

    //successively parse the lines containing the timestamps for the falling edges on each channel
    for(int chInd = 0; chInd<P; ++chInd)
    {
        std::getline(inputF, line); // --> line "Channel #"
        std::cout << "is it Chan#? " << line << std::endl;
        std::getline(inputF, line); // --> line with edges on chInd

        iss_t.clear();
        iss_t.str(line);
        k = 0;
        if(fallingE_outVec[chInd].size()>0)//there is at least 1 edge on this channel
        {
            std::getline(iss_t, timestamp, '\t');
            do
            {
                std::cout << atof(timestamp.c_str()) << ", k=" << k << std::endl;
                if(k<fallingE_outVec[chInd].size())
                    fallingE_outVec[chInd][k].timestamp = atof(timestamp.c_str());
                ++k;
                std::getline(iss_t, timestamp, '\t');
            }while(!iss_t.eof());
        }
        std::getline(inputF, line);// --> empty line
    }

    //set the right parameters for plotting the sequence data and reset all plot data to 0
    ui->seqLength->setValue(L);
    ui->seqTimeStep->setValue(dt);
    for (int i=0; i<8; ++i)
    {
        seqEditors_in_GUI[i]->resetData(dt, (L-1)*dt);
    }


    //now set the plot data with the right edges in each channel using
    //the same routine as the parsing and plotting from QCPSequenceData
    //and replot each channel
    for (int k = 0; k<P; ++k)
    {
        if(risingE_outVec[k].size()!=fallingE_outVec[k].size())
        {
            std::cout << "Error: number of rising and falling edges not equal!\n" << "Add an adequate number of raising and falling edges!\n\n" << std::endl;
            return false;
        }

        QCPGraphData* found_plot_rise = seqEditors_in_GUI[k]->seqData->data()->begin();
        QCPGraphData* found_plot_fall = seqEditors_in_GUI[k]->seqData->data()->begin()-1;
        for (int i=0; i<risingE_outVec[k].size(); ++i)
        {
                //get the i-th rising edge
                double risingEdge = risingE_outVec[k][i].timestamp;
                //get the corresponding fallingEdge
                double fallingEdge = fallingE_outVec[k][i].timestamp;
                if (fallingEdge <= risingEdge)//check that the falling edge is correct
                {
                    std::cout << "Error: corresponding falling edge comes before rising edge!\n" << std::endl;
                    return false;
                }
                //find the index of the rising edge in the plot data(first falling edge "is index -1")
                double Tedge = risingEdge;
                auto testEdge = [&Tedge](QCPGraphData data){return std::abs(data.key-Tedge)<0.0001;};
                found_plot_rise = std::find_if(seqEditors_in_GUI[k]->seqData->data()->begin(), seqEditors_in_GUI[k]->seqData->data()->end(), testEdge);
                //find the index of the falling edge in the plot data
                Tedge = fallingEdge;
                found_plot_fall = std::find_if(seqEditors_in_GUI[k]->seqData->data()->begin(), seqEditors_in_GUI[k]->seqData->data()->end(), testEdge);
                std::for_each(found_plot_rise+1, found_plot_fall+1, [](QCPGraphData& data){(&data)->value = 1;});
                //add a point at the rising edge and at the falling edge
                QVector<double> new_keys = {risingEdge, fallingEdge};
                QVector<double> new_values = {1., 0.};
                seqEditors_in_GUI[k]->seqData->addData(new_keys, new_values, true);
                std::cout << "After add_data:\n" << "found_plot_rise: " << found_plot_rise << '\n' << "found_plot_fall: " << found_plot_fall << std::endl;
                //replot
                seqEditors_in_GUI[k]->replot();

        }
    }

    return true;
}


void MainWindow::on_initM2i_clicked()
{
    M2iInit();
}

void MainWindow::on_confM2i_clicked()
{
    int actChN = next_power_of_2(ui->chanNumber->value());
    std::cout << next_power_of_2(ui->chanNumber->value()) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    int loop_nb = ui->loopNb->value();
    int frequency_MHz = (int)(1000./ui->chan0->step);
    int length_in_samples = lround(ui->chan0->Tmax/ui->chan0->step) + 1;

    M2iSetup(mask, frequency_MHz, false, false, loop_nb, length_in_samples);
}

void MainWindow::on_loadData_clicked()
{
    //M2iLoadIntoMem_testing();

    M2iLoadIntoMemory(ui->chanNumber->value());
}

void MainWindow::on_outputM2i_clicked()
{
    M2iOutputData();
}

void MainWindow::on_translateTest_clicked()
{
    int n = ui->chanNumber->value();
    generateTestData(n, true);
}

void MainWindow::on_closeM2i_clicked()
{
    M2iClose();
}

void MainWindow::on_setPermWord_clicked()
{
    int W = 0;
    for (int c=0; c<8; ++c)
    {
        W += (1<<c)*constStates_in_GUI[c]->isChecked();
    }
    //std::cout << integ_to_binStr((uint16_t) W) << std::endl;

    M2iSetConstLvl(8, (uint16_t) W);
}

void MainWindow::on_parseButton0_clicked()
{
    ui->chan0->seqData->parseAndPlot();
}

void MainWindow::on_parseButton1_clicked()
{
    ui->chan1->seqData->parseAndPlot();
}

void MainWindow::on_parseButton2_clicked()
{
    ui->chan2->seqData->parseAndPlot();
}

void MainWindow::on_parseButton3_clicked()
{
    ui->chan3->seqData->parseAndPlot();
}

void MainWindow::on_parseButton4_clicked()
{
    ui->chan4->seqData->parseAndPlot();
}

void MainWindow::on_parseButton5_clicked()
{
    ui->chan5->seqData->parseAndPlot();
}

void MainWindow::on_parseButton6_clicked()
{
    ui->chan6->seqData->parseAndPlot();
}

void MainWindow::on_parseButton7_clicked()
{
    ui->chan7->seqData->parseAndPlot();
}


void MainWindow::on_writeFileTestButton_clicked()
{
    std::vector< std::vector<edgeProp> > rise;
    std::vector< std::vector<edgeProp> > fall;

    rise.push_back(seqEditors_in_GUI[0]->seqData->risingEdges);
    rise.push_back(seqEditors_in_GUI[1]->seqData->risingEdges);

    fall.push_back(seqEditors_in_GUI[0]->seqData->fallingEdges);
    fall.push_back(seqEditors_in_GUI[1]->seqData->fallingEdges);

    double dt = seqEditors_in_GUI[0]->step;
    int length = lround(seqEditors_in_GUI[0]->Tmax/dt)+1;

    writeSeqToFile(2, length, dt, rise, fall, "E:\\data\\tests_sequences\\", "testFile");
}

void MainWindow::on_loadFileTestButton_clicked()
{
    std::filesystem::path path("E:\\data\\tests_sequences\\");
    std::string name = "2020-07-21 20;03;56 Sequence testFile.dat";

    //now load edges from file into vector ris & fal
    std::vector< std::vector<edgeProp> > ris;
    std::vector< std::vector<edgeProp> > fal;
    loadSeqFromFile(path, name, ris, fal);

    //put the loaded vectors into te rising and falling edges member vectors of the QCPSequenceData
    for (int r = 0; r<8; ++r)
    {
        if(r<ris.size())
            seqEditors_in_GUI[r]->seqData->risingEdges = ris[r];
        else
            seqEditors_in_GUI[r]->seqData->risingEdges.resize(0);
    }
    for (int f =0; f<8; ++f)
    {
        if(f<fal.size())
            seqEditors_in_GUI[f]->seqData->fallingEdges = fal[f];
        else
            seqEditors_in_GUI[f]->seqData->fallingEdges.resize(0);
    }

}

void MainWindow::on_changeSeqParams_clicked()
{
    double Tstep = ui->seqTimeStep->value();
    int seqL = ui->seqLength->value();
    int seqL_rounded;
    if (64*(seqL/64) == seqL)
        seqL_rounded = seqL;
    else
        seqL_rounded = (seqL/64 + 1)*64;
    std::cout << "new length: " << seqL_rounded << std::endl;

    int max_time = (seqL_rounded-1) * Tstep;
    std::cout << "new max T: " << max_time << std::endl;


    //set the plot data to 0 everywhere
    //reset the rising and falling edge vectors to length 0
    for (int i=0; i<8; ++i)
    {
        seqEditors_in_GUI[i]->resetData(Tstep, max_time);
    }
}
