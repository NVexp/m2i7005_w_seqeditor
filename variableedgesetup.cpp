#include "variableedgesetup.h"
#include "ui_variableedgesetup.h"
#include <iostream>

variableEdgeSetup::variableEdgeSetup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::variableEdgeSetup)
{
    ui->setupUi(this);

    std::cout << "New pop up window created!" << std::endl;
}

variableEdgeSetup::~variableEdgeSetup()
{
    delete ui;
}

void variableEdgeSetup::on_buttonBox_accepted()
{
    std::cout << "Working ?" << ui->edgeTimestamp->value() << std::endl;
}
