#ifndef QSEQEDITOR_H
#define QSEQEDITOR_H
#include "qcustomplot.h"
#include "qcpsequencedata.h"


class QSeqEditor : public QCustomPlot
{
    Q_OBJECT
public:
    QSeqEditor(QWidget* parentWidget);
    QCPSequenceData* seqData;
    double step, Tmax;

public slots:
    void printAfterRangeChanged(const QCPRange& range);
    void setNewRange(const QCPRange& range);
    void resetData(double step, double maxTime);

};

#endif // QSEQEDITOR_H
